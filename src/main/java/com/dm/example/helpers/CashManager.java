package com.dm.example.helpers;

import com.dm.example.models.CashResult;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Дмитрий on 10.04.2017.
 */
public class CashManager {

    private final String filePath = "./current.cash";

    public CashResult add(double cash){
        double current = readCurrent();
        current += cash;
        writeCurrent(current);
        return new CashResult(current);
    }

    public CashResult remove(double cash){
        double current = readCurrent();
        current -= cash;
        writeCurrent(current);
        return new CashResult(current);
    }

    public CashResult currentState(){
        return new CashResult(readCurrent());
    }

    private double readCurrent(){
        double current = 0;
        try {
            byte[] buffer = Files.readAllBytes(Paths.get(filePath));
            current = ByteBuffer.wrap(buffer).getDouble();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return current;
    }

    private void writeCurrent(double val){
        try{
            byte[] buffer = new byte[8];
            ByteBuffer.wrap(buffer).putDouble(val);
            Files.write(Paths.get(filePath), buffer);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
