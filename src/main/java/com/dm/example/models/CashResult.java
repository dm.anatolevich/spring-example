package com.dm.example.models;

/**
 * Created by Дмитрий on 10.04.2017.
 */
public class CashResult {
    private double cash;

    public CashResult(double cash){
        this.cash = cash;
    }

    public double getCash() {
        return cash;
    }
}
