package com.dm.example.controllers;

import com.dm.example.helpers.CashManager;
import com.dm.example.models.CashResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Дмитрий on 10.04.2017.
 */
@RestController
public class CashController {

    private CashManager manager = new CashManager();

    @RequestMapping("/add")
    public CashResult add(@RequestParam(value="add", defaultValue="0.0") String add) {
        return manager.add(Double.valueOf(add));
    }

    @RequestMapping("/remove")
    public CashResult remove(@RequestParam(value="remove", defaultValue="0.0") String remove) {
        return manager.remove(Double.valueOf(remove));
    }

    @RequestMapping("/current")
    public CashResult current() {
        return manager.currentState();
    }
}
